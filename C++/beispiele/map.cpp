#include <map>

using namespace std;

int main()
{
    map<string, int> m;
    m["Apfel"] = 1;
    m["Orange"] = 2;
    m["Kirsche"]; // m.count("Kirsche") == 1 (!!!)
}
