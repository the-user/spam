#include <iostream>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);    

    int n; string s;
    cin >> n;
    getline(cin, s);
    for (int i = 0; i < n; ++i) {
        getline(cin, s);
        cout << s << "\n";
    }
}
