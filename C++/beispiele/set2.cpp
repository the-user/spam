#include <iostream>
#include <set>
using namespace std;

int main() {
    set<int> s = {33, 93, 42, 5, 0};
    for (auto it = s.begin(); it != s.end(); ++it) {
        cout << *it << " ";
    }
    cout << "\n";
    for (int i: s) cout << i << " ";
    cout << "\n";
}
