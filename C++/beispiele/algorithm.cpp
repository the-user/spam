#include <vector>
#include <algorithm>
using namespace std;

int main()
{
    vector<int> v;
    sort(v.begin(), v.end());
    sort(v.rbegin(), v.rend());
    sort(v.begin(), v.end(), [](int a, int b){
        return a % 42 < b % 42;
    });
}
