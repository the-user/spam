#include <vector>
using namespace std;
int main() {
    vector<int> v = {1, 42, 3, 4, 5, 42};
    v.pop_back();
    v.push_back(6);
    v[1] = 2;
    vector<int> v2(3, 42);
    vector<int> v3;
    // v = {1, 2, 3, 4, 5, 6}
    // v2 = {42, 42, 42}
    // v3 = {}
}
