#include <queue>
using namespace std;

int main() {
    priority_queue<int, vector<int>, greater<int>> q;
    q.push(3); q.push(9); q.push(7); q.push(8);
    q.top(); // == 3
    q.pop();
    q.top(); // == 7
}
