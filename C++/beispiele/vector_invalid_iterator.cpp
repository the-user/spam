#include <vector>

using namespace std;

int main() {
    vector<int> v = {1, 42, 3, 4, 5, 42};
    for (auto it = v.begin(); it != v.end(); ++it) {
        if (*it == 42) v.push_back(3);
    }
}
