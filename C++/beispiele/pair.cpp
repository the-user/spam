#include <tuple>
using namespace std;

int main() 
{
    pair<int, char> p = {1, 'a'};
    auto p2 = make_pair(1, 'a');
    tuple<char, int, int> t = make_tuple('a', 42, -1);
    int i; char c;
    tie(c, ignore, i) = t;
}
