#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    const double pi = 3.014159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230664709384;
    cout << pi << "\n";
    cout << setprecision(10) << pi << "\n";
}
