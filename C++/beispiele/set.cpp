#include <set>
using namespace std;

int main() {
    set<int> s = {33, 93, 42, 5, 0};
    s.count(42); // == 1
    s.count(-3); // == 0
    s.insert(42); // == false
    s.count(42); // == 1
    s.insert(-3); // == true
    s.erase(-3); // == true
}
