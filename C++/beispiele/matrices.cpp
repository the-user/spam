#include <bits/stdc++.h>

using namespace std;

typedef vector<vector<int>> matrix;

int main()
{
    int n = 2000;
    matrix a(n, vector<int>(n));
    matrix result = a;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            a[i][j] = i + j;
        }
    }

    for (int i = 0; i < n; ++i) {
        for (int k = 0; k < n; ++k) {
            for (int j = 0; j < n; ++j) {
                result[i][j] += a[i][k] * a[k][j];
            }
        }
    }
}
