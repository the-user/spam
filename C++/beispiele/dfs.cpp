#include <bits/stdc++.h>

using namespace std;

void dfs(vector<vector<int>> adj, vector<bool> visited, int from)
{
    if (visited[from]) return;
    visited[from] = true;
    for (int next: adj[from]) dfs(adj, visited, next);
}
