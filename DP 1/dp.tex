\documentclass[12pt,compress]{beamer}
\usepackage[ngerman]{babel}
\usepackage{ucs}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{uniinput}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{tikz}

\definecolor{bluekeywords}{rgb}{0.13,0.13,1}
\definecolor{greencomments}{rgb}{0,0.5,0}
\definecolor{redstrings}{rgb}{0.9,0,0}
\lstset{language=C++,
numbers=left,
stepnumber=1,
showspaces=false,
showtabs=false,
breaklines=true,
showstringspaces=false,
breakatwhitespace=true,
commentstyle=\color{greencomments},
keywordstyle=\color{bluekeywords}\bfseries,
stringstyle=\color{redstrings},
basicstyle=\ttfamily,
moredelim=**[is][\color{brown}]{@}{@},
moredelim=**[is][\color{purple}]{§}{§},
moredelim=**[is][\color{Mahogany}]{§§}{§§},
}
\lstset{literate=%
{Ö}{{\"O}}1
{Ä}{{\"A}}1
{Ü}{{\"U}}1
{ß}{{\ss}}2
{ü}{{\"u}}1
{ä}{{\"a}}1
{ö}{{\"o}}1
}

\addto\captionsngerman{\renewcommand{\figurename}{Abb.}}

\title{Dynamische Programmierung}
\subtitle{Eine Einführung}
\author{Philip Wellnitz}
\date{\today}

\useoutertheme[subsection=false]{miniframes}
\usecolortheme{dove}
\usefonttheme[onlysmall]{structurebold}

\begin{document}
\setbeamercovered{transparent}
    \begin{frame}[plain]
        \titlepage%
    \end{frame}

    \title{DP—Eine Einführung}
    \begin{frame}[plain]{Gliederung}
        \tableofcontents
    \end{frame}

    \section{Motivation und Grundideen}
    \subsection{Fibonacci-Zahlen}

    \begin{frame}{{\small\insertsubsectionhead\\}Aufgabe}
        \begin{itemize}
            \item \emph{Fibonacci-Zahl} $Fib(n)$:\[
                Fib(n) := \left\{\begin{array}{c c}
                        1, & n < 2\\
                        Fib(n-1) + Fib(n-2), & n \ge 2
                    \end{array}\right.
                \]
            \item<1>{\small Beispiele:\vspace{-1em}\begin{align*}
                    Fib( 4 ) &= 5\\
                    Fib( 5 ) &= 8\\
                    Fib( 6 ) &= 13
                \end{align*}}
            \item<2-> Gegeben $n$, berechne $Fib(n)$.
        \end{itemize}
    \end{frame}
    \begin{frame}[containsverbatim]{{\small\insertsubsectionhead\\}Naive Lösung}
            Umsetzung der Definition:\\
            \begin{lstlisting}
long long fib( int n ) {
    if( n < 2 ) return 1;
    return fib( n-1 ) + fib( n-2 );
}
        \end{lstlisting}
    \end{frame}
    \begin{frame}{{\small\insertsubsectionhead\\}Naive Lösung}
        Berechnung für $Fib(5)$:{\footnotesize\begin{alignat*}{10}
                &&&&&&&&&&&\!\!\!Fib(5)\\
                &= &&&&&&Fib( 4 )&& &&\,+ &&&&Fib( 3 )\\
                 &= &&&&Fib( 3 ) &&\,+ &&Fib( 2 ) &&\,+ &&Fib( 2 ) &&+ Fib( 1 )\\
                 &= &&Fib( 2 ) &&+ Fib( 1 ) &&+ Fib( 1 ) &&+ Fib( 0 )
    &&+ Fib( 1 ) &&+ Fib( 0 ) &&+ \quad1\\
    &= Fib( 1 ) &&+ Fib( 0 ) &&+\quad1&&+\quad1 &&+\quad1 &&+\quad1&&+ \quad1 &&+\quad1\\
    &= \quad1 &&+ \quad1 &&+ \quad1 &&+ \quad1 &&+ \quad1 &&+ \quad1 &&+ \quad1 &&+ \quad1\\
    &= &&&&&&&&&&\,\,8
        \end{alignat*}}
    \end{frame}
    \begin{frame}[containsverbatim]{{\small\insertsubsectionhead\\}Schnelle Lösung}
        Doppelte Berechnungen sind doppelt:\\\begin{lstlisting}
long long mem[ 1000000 ];
long long fib( int n ) {
    if( n < 2 ) return 1;
    long long& m = mem[ n ];
    if( m != -1 ) return m;
    m = fib( n-1 ) + fib( n-2 );
    return m;
}
int main( ) {
    memset( mem, -1, sizeof( mem ) );
    //...
}
        \end{lstlisting}
    \end{frame}
    \begin{frame}{{\small\insertsubsectionhead\\}Schnelle Lösung}
        Berechnung für $Fib(5)$:{\begin{alignat*}{10}
                &&&&&&&&&&&\!\!\!\!Fib(5)\\
                 &= &&&&&&Fib( 4 )&& &&+ &&&&Fib( 3 )\\
                 &= &&&&Fib( 3 ) &&+ &&Fib(2)\quad &&+ &&&&\quad3\\
                 &= &&Fib( 2 ) &&+ Fib( 1 ) &&+ &&\quad2 &&+ &&&&\quad3\\
                 &= Fib( 1 ) &&+ Fib( 0 ) &&+\quad1&&+&&\quad2 &&+&&&&\quad3\\
                 &= \quad1 &&+ \quad1 &&+ \quad1 &&+ &&\quad2 &&+ &&&&\quad3\\
    &= &&&&&&&&&&\,8
        \end{alignat*}}
    \end{frame}
    \subsection{Bottom-Up DP, Top-Down DP}
    \begin{frame}{{\small\insertsubsectionhead}}
        \begin{columns}
            \begin{column}{.6\textwidth}
                \begin{center}\small
                    \begin{figure}
                        \begin{tikzpicture}
                            \node (f5) [color=red] at (.125,0) {$Fib(5)$};
                            \node (f4) [color=red] at (-.5,-1) {$Fib(4)$};
                            \node (f3) [color=red] at (-1.75,-2) {$Fib(3)$};
                            \node (f2) [color=red] at (-2.9,-3) {$Fib(2)$};
                            \node (f1) [color=red] at (-2.9,-4) {$Fib(1)$};
                            \node (f0) [color=red] at (-1.75,-4) {$Fib(0)$};
                            \node (f12) at (-1.75,-3) {$Fib(1)$};
                            \node (f23) at (-.5,-2) {$Fib(2)$};
                            \node (f13) at (-.9,-3.5) {$Fib(1)$};
                            \node (f03) at (-.155,-4) {$Fib(0)$};
                            \node (f35) at (1.25,-1) {$Fib(3)$};
                            \node (f25) at (.75,-2) {$Fib(2)$};
                            \node (f15) at (.45,-3.5) {$Fib(1)$};
                            \node (f05) at (1.2,-3) {$Fib(0)$};
                            \node (f16) at (2,-3.5) {$Fib(1)$};

                            \node (1) [draw, circle] at (0,-5) {$1$};

                            \path (f25.south) edge [ -> ] (f05.north);
                            \path (f25.south) edge [ -> ] (f15.north);
                            \path (f35.south) edge [ -> ] (f25.north);
                            \path (f35.south) edge [ -> ] (f16.north);
                            \path (f5.south) edge [ -> ] (f35.north);
                            \path (f4.south) edge [ -> ] (f23.north);
                            \path (f23.south) edge [ -> ] (f03.north);
                            \path (f23.south) edge [ -> ] (f13.north);
                            \path (f5.south) edge [ -> ] (f4.north);
                            \path (f4.south) edge [ -> ] (f3.north);
                            \path (f3.south) edge [ -> ] (f2.north);
                            \path (f3.south) edge [ -> ] (f12.north);
                            \path (f2.south) edge [ -> ] (f1.north);
                            \path (f2.south) edge [ -> ] (f0.north);

                            \path (f16.south) edge [ -> ] (1.east);
                            \path (f15.south) edge [ -> ] (1.north east);
                            \path (f05.south) edge [ -> ] (1.north east);
                            \path (f12.south) edge [ -> ] (1.north west);
                            \path (f13.south) edge [ -> ] (1.north west);
                            \path (f03.south) edge [ -> ] (1.north);
                            \path (f1.south) edge [ -> ] (1.west);
                            \path (f0.south) edge [ -> ] (1.north west);
                        \end{tikzpicture}
                        \caption{Naive Lösung: $O(Fib(n))$}
                    \end{figure}
                \end{center}
            \end{column}
            \begin{column}{.45\textwidth}
                \begin{center}\small
                    \begin{figure}
                        \begin{tikzpicture}
                            \node (f5) at (0,0) {$Fib(5)$};
                            \node (f4) at (0,-1) {$Fib(4)$};
                            \node (f3) at (0,-2) {$Fib(3)$};
                            \node (f2) at (0,-3) {$Fib(2)$};
                            \node (f1) at (1,-4) {$Fib(1)$};
                            \node (f0) at (-1,-4) {$Fib(0)$};
                            \node (1) [draw, circle] at (0,-5) {$1$};

                            \path (f5.east) edge [ ->, bend left ] (f4.east);
                            \path (f5.east) edge [ ->, bend left ] (f3.east);
                            \path (f4.west) edge [ ->, bend right ] (f3.west);
                            \path (f4.west) edge [ ->, bend right ] (f2.west);
                            \path (f3.east) edge [ ->, bend left ] (f2.east);
                            \path (f3.east) edge [ ->, bend left ] (f1.north);
                            \path (f2.east) edge [ ->, bend left ] (f1.north);
                            \path (f2.west) edge [ ->, bend right ] (f0.north);
                            \path (f1.south) edge [ ->, bend left ] (1.east);
                            \path (f0.south) edge [ ->, bend right ] (1.west);
                        \end{tikzpicture}
                        \caption{Schnelle Lösung: $O(n)$}
                    \end{figure}
                \end{center}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}{{\small\insertsubsectionhead}}
        \begin{columns}
            \begin{column}{.5\textwidth}
                \begin{center}\small
                    \begin{figure}
                        \begin{tikzpicture}
                            \node (f5) at (0,0) {$Fib(5)$};
                            \node (f4) at (0,-1) {$Fib(4)$};
                            \node (f3) at (0,-2) {$Fib(3)$};
                            \node (f2) at (0,-3) {$Fib(2)$};
                            \node (f1) at (1,-4) {$Fib(1)$};
                            \node (f0) at (-1,-4) {$Fib(0)$};
                            \node (1) [draw, circle] at (0,-5) {$1$};

                            \path (f5.east) edge [ ->, bend left ] (f4.east);
                            \path (f5.east) edge [ ->, bend left ] (f3.east);
                            \path (f4.west) edge [ ->, bend right ] (f3.west);
                            \path (f4.west) edge [ ->, bend right ] (f2.west);
                            \path (f3.east) edge [ ->, bend left ] (f2.east);
                            \path (f3.east) edge [ ->, bend left ] (f1.north);
                            \path (f2.east) edge [ ->, bend left ] (f1.north);
                            \path (f2.west) edge [ ->, bend right ] (f0.north);
                            \path (f1.south) edge [ ->, bend left ] (1.east);
                            \path (f0.south) edge [ ->, bend right ] (1.west);
                        \end{tikzpicture}
                        \caption{„Top-Down“ DP}
                    \end{figure}
                \end{center}
            \end{column}
            \begin{column}{.5\textwidth}
                \begin{center}\small
                    \begin{figure}
                        \begin{tikzpicture}
                            \node (f5) at (0,0) {$Fib(5)$};
                            \node (f4) at (0,-1) {$Fib(4)$};
                            \node (f3) at (0,-2) {$Fib(3)$};
                            \node (f2) at (0,-3) {$Fib(2)$};
                            \node (f1) at (1,-4) {$Fib(1)$};
                            \node (f0) at (-1,-4) {$Fib(0)$};
                            \node (1) [draw, circle] at (0,-5) {$1$};

                            \path (f5.east) edge [ <-, bend left ] (f4.east);
                            \path (f5.east) edge [ <-, bend left ] (f3.east);
                            \path (f4.west) edge [ <-, bend right ] (f3.west);
                            \path (f4.west) edge [ <-, bend right ] (f2.west);
                            \path (f3.east) edge [ <-, bend left ] (f2.east);
                            \path (f3.east) edge [ <-, bend left ] (f1.north);
                            \path (f2.east) edge [ <-, bend left ] (f1.north);
                            \path (f2.west) edge [ <-, bend right ] (f0.north);
                            \path (f1.south) edge [ <-, bend left ] (1.east);
                            \path (f0.south) edge [ <-, bend right ] (1.west);
                        \end{tikzpicture}
                        \caption{„Bottom-Up“ DP}
                    \end{figure}
                \end{center}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}[containsverbatim]{{\small\insertsubsectionhead}\\Bottom-Up: Implementierung}
        \begin{lstlisting}
int main( ) {
    int n;
    //...
    long long mem[ 1000000 ] = { 0 };
    mem[ 0 ] = mem[ 1 ] = 1;
    for( int i = 0; i < n; ++i ) {
        mem[ i + 1 ] += mem[ i ];
        mem[ i + 2 ] += mem[ i ];
    }
    long long res = mem[ n ];
}
        \end{lstlisting}
    \end{frame}
    \begin{frame}[containsverbatim]{{\small\insertsubsectionhead}\\Bottom-Up: Implementierung}
        \begin{lstlisting}
int main( ) {
    int n;
    //...
    long long mem[ 1000000 ] = { 0 };
    mem[ 0 ] = mem[ 1 ] = 1;
    for( int i = 2; i <= n; ++i ) {
        mem[ i ] += mem[ i - 1 ];
        mem[ i ] += mem[ i - 2 ];
    }
    long long res = mem[ n ];
}
        \end{lstlisting}
    \end{frame}
    \begin{frame}[containsverbatim]{{\small\insertsubsectionhead}\\Bottom-Up: Implementierung}
        \begin{lstlisting}
int main( ) {
    int n;
    //...
    //Kein mem!
    long long a = 1, b = 1;
    for( int i = 2; i <= n; ++i ) {
        long long c = a + b;
        a = b; b = c;
    }
    long long res = b;
}
        \end{lstlisting}
    \end{frame}


    \section{Ausgewählte Standardaufgaben}
    \subsection{Längste (streng) monotone Teilfolge}

    \newcommand{\n}{\phantom{0}}
    \begin{frame}{{\small\insertsubsectionhead\\}Aufgabe}
        \begin{itemize}
            \item\alert{Gegeben:} Array $A[\,0..n-1\,]$ an $n$ Zahlen
            \item\alert{Gesucht:} Menge $M \subseteq \{0,…,n-1\}$, mit $|M|$ maximal und\[
                    \underset{i,j,∈M}{∀i,j:} i < j ⇒ A[\,i\,] \leq A[\,j\,]
                \]
            \only<-2>{
            \item<2> Beispiel:\\\hfill{\tt 1 -1 5 -6 8 -4 15 5 -3}\hfill\mbox{}
            }
            \only<3->{
        \item<3> Beispiel:\\\hfill{\tt 1 {\color{red}-1 5} -6 {\color{red}8} -4 {\color{red}15}
        5 -3}
                \hfill\mbox{}
            }
        \end{itemize}
    \end{frame}
    \begin{frame}{{\small\insertsubsectionhead\\}Lösung}
        \begin{itemize}
            \item<1> Naiv: alle Teilmengen, $O(2^n)$
            \item<2-> Idee: Erst Länge, dann Rekonstruktion
            \item<2-> $λ(i)$: Lösung $|M_i|$ für $A[\,0..i\,]$ und $i ∈ M_i$,\[
                    λ(i) = \max\left\{
                        \underset{A[\,j\,]\le A[\,i\,]}{\underset{0\le j<i}{\max}}\{1 + λ(j)\},
                        1\right\}
                \]
            \item<2-> Lösung: $\underset{0\le i<n}{\max}\{λ(i)\}$
            \item<3-> {\tt\begin{tabular}{c r r r r r r r r r}
                        $A$ & 1 & -1 & 5 & -6 & 8 & -4 & 15 & 5 & -3\\ \uncover<3->{$λ(i)$} &
                        \only<3>{{\color{red}1}}%
                        \only<4>{1 & {\color{red}1}}%
                        \only<5>{{\color{blue} 1} & {\color{blue}1} & {\color{red}2}}%
                        \only<6>{1 & 1 & 2 & {\color{red}1}}%
                        \only<7>{1 & 1 & {\color{blue}2} & 1 & {\color{red}3}}%
                        \only<8>{1 & {\color{blue}1} & 2 & 1 & 3 & {\color{red}2}}%
                        \only<9>{1 & 1 & 2 & 1 & {\color{blue}3} & 2 & {\color{red}4}}%
                        \only<10>{1 & 1 & 2 & 1 & 3 & {\color{blue}2} & 4 & {\color{red}3}}%
                        \only<11>{1 & {\color{blue}1} & 2 & 1 & 3 & 2 & 4 & 3 & {\color{red}2}}%
                        \only<12>{1 & 1 & 2 & 1 & 3 & 2 & {\color{red}4} & 3 & 2}%
        \end{tabular}}
        \end{itemize}
    \end{frame}
    \begin{frame}{{\small\insertsubsectionhead\\}Lösung}
        \begin{itemize}
            \item<-5> Rekonstruktion: Finde kleineren Index $j$ mit \[λ(j) + 1 = λ(i)\]
            \item<2-5> {\tt\begin{tabular}{c r r r r r r r r r}
                        $A$ & 1 & -1 & 5 & -6 & 8 & -4 & 15 & 5 & -3\\
            \only<2>{$λ(i)$ &   1 & 1 & 2 & 1 & 3 & 2 & {\color{red}4} & 3 & 2\\}%
            \only<3>{$λ(i)$ &   1 & 1 & 2 & 1 & {\color{red}3} & 2 & 4 & 3 & 2\\}%
            \only<4>{$λ(i)$ &   1 & 1 & {\color{red}2} & 1 & 3 & 2 & 4 & 3 & 2\\}%
            \only<5>{$λ(i)$ &   1 & {\color{red}1} & 2 & 1 & 3 & 2 & 4 & 3 & 2\\}%
            \only<6->{$λ(i)$ &   1 & 1 & 2 & 1 & 3 & 2 & 4 & 3 & 2\\}%
                        $_i$ & $_0$ & $_1$ & $_2$ & $_3$ & $_4$ & $_5$ & $_6$ & $_7$ & $_8$
                \end{tabular}}
            \only<2>{\item Lösung: {\tt 6}}%
            \only<3>{\item Lösung: {\tt 4, 6}}%
            \only<4>{\item Lösung: {\tt 2, 4, 6}}%
            \only<5->{\item<5> Lösung: {\tt 1, 2, 4, 6}}%

        \item<6-> Berechnung $λ(i)$: analog $Fib(n)$!
        \item<7-> Laufzeit: $O(n² + n)$
        \end{itemize}
    \end{frame}


    \subsection{Rucksackproblem}

    \begin{frame}{{\small\insertsubsectionhead\\}Aufgabe}
        \begin{itemize}
            \item \alert{Gegeben:}
                Rucksack mit Kapazität $K$\\
                $n$ verschiedene Objekte $O_i$ mit Werten $W_i$ und Größen $G_i$
            \item \alert{Gesucht:} Maximaler Gewinn
            \item<2-> Beispiel:\\ Kapazität $K=11$:\\
                \only<-2>{\begin{center}{\tt\begin{tabular}{c |r|r|r|r|}
                        \cline{2-5}
                    $W_i$ & 100 & 70 & 50 & 10\\
                    $G_i$ & 10 & 4 & 6 & 12\\
                        \cline{2-5}
            \end{tabular}}\end{center}}
                \only<3>{\begin{center}{\tt\begin{tabular}{c |r|r|r|r|}
                        \cline{2-5}
                        $W_i$ & 100 & {\color{red}70} & {\color{red}50} & 10\\
                        $G_i$ & 10 & {\color{red}4} & {\color{red}6} & 12\\
                        \cline{2-5}
            \end{tabular}}\end{center}}
        \end{itemize}
    \end{frame}
    \begin{frame}{{\small\insertsubsectionhead\\}Varianten}
        \begin{itemize}
            \item 1 Objekt pro Sorte:\\\emph{0-1-Rucksackproblem}
            \item $k$ Objekte pro Sorte:\\\emph{ganzzahliges Rucksackproblem}
            \item beliebig viele Objekte pro Sorte:\\
                \emph{ganzzahliges Rucksackproblem (unbeschränkt)}
            \item Objekte dürfen zerteilt werden: \\\emph{rationales Rucksackproblem}
                (langweilig)
        \end{itemize}
    \end{frame}
    \begin{frame}{{\small 0-1-Rucksackproblem\\}Lösung}
        \begin{itemize}
            \item Gewinn mit $O_{k+1},…,O_n$ und Restkapazität $r$: $g( k, r )$
            \item Basisfälle: $g( n, \_ ) = 0$, $g( \_, 0 ) = 0$, $g(\_,<0) = -∞$
            \item Rekursionsgleichung:\[
                    g( k, r ) = \max\{ g( k + 1, r ), W_k + g( k + 1, r - G_k )\}
                    \]
            \item Laufzeit: $O(K\cdot n)$
        \end{itemize}
    \end{frame}
    \begin{frame}{{\small Ganzzahliges Rucksackproblem\\}Lösung}
        \begin{itemize}
            \item $\#i$: Anzahl von Objekt $i$
            \item Gewinn mit $O_{k+1},…,O_n$ und Restkapazität $r$: $g( k, r )$
            \item Basisfälle: $g( n, \_ ) = 0$, $g( \_, 0 ) = 0$, $g(\_,<0) = -∞$
            \item Rekursionsgleichung:\[
                    g( k, r ) = \underset{0\le j\le\#k}{\max}\{
                    j \cdot W_k + g( k + 1, r - j \cdot G_k )\}
                    \]
            \item Laufzeit: $O(K²\cdot n)$
        \end{itemize}
    \end{frame}
    \begin{frame}{{\small Ganzzahliges Rucksackproblem (unbeschränkt)\\}Lösung}
        \begin{itemize}
            \item Gewinn mit $O_{k+1},…,O_n$ und Restkapazität $r$: $g( k, r )$
            \item Basisfälle: $g( n, \_ ) = 0$, $g( \_, 0 ) = 0$, $g(\_, <0) = -∞$
            \item Rekursionsgleichung:\[
                    g( k, r ) = \underset{r-j \cdot G_k \ge 0}{\max}\{
                    j \cdot W_k + g( k + 1, r - j \cdot G_k )\}
                    \]
            \item Laufzeit: $O(K²\cdot n)$
        \end{itemize}
    \end{frame}
    \begin{frame}{{\small Ganzzahliges Rucksackproblem (unbeschränkt)\\}Lösung (besser)}
        \begin{itemize}
            \item Gewinn mit Restkapazität $r$: $g( r )$
            %\item Letztes Objekt im Rucksack der Größe $r$: $o( r )$
            \item Basisfälle: $g( 0 ) = 0$, $g( <0) = -∞$
            \item Rekursionsgleichung:\[
                    g( r ) = \underset{0\le k< n}{\max}\{W_k + g( r - G_k )\}\]
            \item Laufzeit: $O(K\cdot n)$
            \item<2-> Denken statt abschreiben!
        \end{itemize}
    \end{frame}

    \subsection{Editierdistanz}

    \begin{frame}{{\small\insertsubsectionhead\\}Aufgabe}
        \begin{itemize}
            \item \alert{Gegeben:} 2 Strings $a₁a₂…a_n$, $b₁b₂…b_m$
            \item \alert{Gesucht:} Minimale Anzahl an Umformungen $a\leadsto b$
            \item Umformungen:
                \begin{itemize}
                    \item Zeichen entfernen, $DELETE = 1$
                    \item Zeichen einfügen, $INSERT = 1$
                    \item Zeichen verändern, $CHANGE = 1$
                    \item Zeichen belassen, $MATCH = 0$
                \end{itemize}
        \item<2-> Beispiel: {\tt NERV} $\leadsto$ {\tt SEELE}\\
        \begin{center}{\tt\begin{tabular}{r r r r r r r r r r}
            \only<2-3>{&N&E&R&V&&&&&$0$\\}%
                \only<3-4>{{\color{red}S}&N&E&R&V&&&&&$1$\\}%
                \only<4-5>{S&{\color{red}E}&E&R&V&&&&&$2$\\}%
                \only<5-6>{S&E&E&{\color{red}L}&V&&&&&$3$\\}%
                \only<6-7>{S&E&E&L&{\color{red}E}&&&&&$4$\\}%
                \only<7>{S&E&E&L&E&&&&&\\}%
        \end{tabular}}\end{center}
\end{itemize}
    \end{frame}
    \begin{frame}{{\small\insertsubsectionhead\\}Lösung}
        \begin{itemize}
            \item Erst Kosten, dann Rekonstruktion
            \item Lösung für $a[0..i]$ und $b[0..j]$: $e(i,j)$
            \item
                $ε\leadsto b[0..j]$: $e(0,j) = j\cdot INSERT$\\
                $a[0..i]\leadsto ε$: $e(i,0) = i\cdot DELETE$
            \item<2-> Rekursionsgleichung:
                \begin{align*}
                    e(i,j) = \min\{&e(i-1,j-1)+diff(a_i,b_j),\\
                           &e(i-1,j) + INSERT,\\
                           &e(i,j-1) + DELETE \},
                \end{align*}\[
                diff(a,b) = \left\{\begin{array}{c c}MATCH,&a = b\\CHANGE,&a≠b\end{array}\right.
                \]
        \end{itemize}
    \end{frame}
    \begin{frame}{{\small\insertsubsectionhead\\}Lösung}
        \begin{itemize}
            \item<1> Rekonstruktion:\begin{itemize}
                    \item $e(i,j)=e(i-1,j)+INSERT$ ⇒ Einfügen
                    \item $e(i,j)=e(i,j-1)+DELETE$ ⇒ Löschen
                    \item $e(i,j)=e(i-1,j-1)+diff(a_i, b_j)$ ⇒ Ändern
                \end{itemize}
            \item<1> Laufzeit: $O(|a|\cdot|b|) = O(m\cdot n)$
            \item<2-> Längster gemeinsamer Teilstring:
                \begin{align*}
                    DELETE &= 0\\
                    INSERT &= 0\\
                    CHANGE &= ∞\\
                    MATCH &= -1
                \end{align*}
        \end{itemize}
    \end{frame}

    \section{Aufgaben}
    \subsection{Weitere Standardaufgaben (und -anwendungen)}
    \begin{frame}{\insertsubsectionhead}
        \begin{itemize}
            \item Wechselgeldproblem (Coin Change)
            \item Egg-Dropping-Puzzle\\[1em]
            \item Binomialkoeffizient
            \item Algorithmus von Dijkstra (kürzeste Pfade)
            \item Algorithmus von Floyd-Warshall (kürzeste Pfade)\\[1em]
            \item Gerichtete azyklische Graphen (DAG)
            \item Allgemein: Doppelte Berechnungen von Teilproblemen
            \item …
            %\item Algorithmus von Cocke-Younger-Kasami (Wortproblem NKA)
        \end{itemize}
    \end{frame}

    \subsection{Übungen}
    \begin{frame}{\insertsectionhead}
        \begin{itemize}
            \item Einfach verschiedene Aufgaben Unit-00:
                \begin{itemize}
                    \item UVa: 00497, 00990, 10664, 00164, 10405, 10066
                    \item {\bf TUMjudge}
                \end{itemize}
            \item Einfach verschiedene Aufgaben Unit-01:
                \begin{itemize}
                    \item UVa: 11456, 00147, 10003, 11703
                    \item {\bf TUMjudge}
                \end{itemize}
            \item Einfach verschiedene Aufgaben Unit-02:
                \begin{itemize}
                    \item \alert{vim} (BOI 2013): \url{http://boi2013.informatik-olympiade.de/wp-content/uploads/2013/04/vim-DEU.pdf}
                \end{itemize}
        \end{itemize}
    \end{frame}
\end{document}
