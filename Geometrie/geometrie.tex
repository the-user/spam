%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright (C) 2013 by Jonathan Schmidt-Dominé                              %%
%%                                                                            %%
%% This program is free software: you can redistribute it and/or modify it    %%
%% under the terms of the GNU Affero General Public License as published by   %%
%% the Free Software Foundation, either version 3 of the License, or (at your %%
%% option) any later version.                                                 %%
%%                                                                            %%
%% This program is distributed in the hope that it will be useful, but        %%
%% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY %%
%% or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public     %%
%% License for more details.                                                  %%
%%                                                                            %%
%% You should have received a copy of the GNU Affero General Public License   %%
%% along with this program. If not, see <http://www.gnu.org/licenses/>.       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}

\usepackage[ngerman]{babel}

\usepackage{savesym}

\savesymbol{approx}
\savesymbol{colonapprox}
\savesymbol{colonsim}

\usepackage{colonequals} %colonequals ≔
\usepackage{ulsy} %blitza

\usepackage{iftex}
\ifPDFTeX
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage{uniinput} % http://wiki.neo-layout.org/browser/latex/Standard-LaTeX
  \usepackage{lmodern}
  \usepackage{amsmath}
  \usepackage{amssymb}
\else
  \ifLuaTeX
    \usepackage{luatextra}
    \usepackage{amsmath}
    \usepackage{amssymb}
    \usepackage{lmodern}
    \usepackage{uniinput-lualatex} % http://files.the-user.org/uniinput-lualatex.sty
    \usepackage{fontspec}
  \fi
\fi

\usepackage{amsthm}

\beamertemplatenavigationsymbolsempty
\usetheme{Anadyr}
\newcommand{\hideinstitute}{}
\renewcommand{\footerauthorwidth}{0.28}
\renewcommand{\footertitlewidth}{0.44}
\renewcommand{\footerdatewidth}{0.28}
\newcommand{\blackframe}{{\setbeamercolor{normal text}{bg=black!100} \frame[plain]{}}}

\usepackage{hyperref}

\usepackage{float}

\usepackage{tikz}
\usetikzlibrary{shapes,arrows}

\usepackage{listings}
\lstset{
  language=C++
}

\newcommand{\symdiff}{\ensuremath{\ominus}} % some math symbols
\newcommand{\dsum}{\ensuremath{\bigoplus}}
\newcommand{\ps}[1]{\ensuremath{\mathcal P\left(#1\right)}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\acos}{\ensuremath{\operatorname{acos}}}
\newcommand{\atan}{\ensuremath{\operatorname{atan}}}
\newcommand{\atantwo}{\ensuremath{\operatorname{atan2}}}


\savesymbol{corollary}
\savesymbol{lemma}
\savesymbol{theorem}
\newcounter{satz}
\newcounter{definition}
\newtheorem{lemma}[satz]{Lemma}
\newtheorem{corollary}[satz]{Korollar}
\newtheorem{theorem}[satz]{Satz}
\theoremstyle{definition}
\newtheorem{defi}[definition]{Definition}

\title{Geometrische Algorithmen}
\author{Jonathan Schmidt-Dominé}
\institute{}

\setcounter{tocdepth}{1} % short tocs are usually better in presentations

\begin{document}

\frame{
  \titlepage
}

\frame[allowframebreaks]{
  \tableofcontents
}

\section{Analytische Geometrie}

\subsection{Vektoren}
\frame{
\begin{itemize}
 \item<+-> Wir fassen Punkte als Koordinatenvektoren auf.
 \item<+-> Auf ihnen sind Addition und Skalarmultiplikation wie üblich komponentenweise definiert.
\end{itemize}

}

\subsection{Metrik}
\frame{
\begin{itemize}
 \item<+-> Das Standardskalarprodukt ist definiert als:
 \[
  \vec{x}·\vec{y}≔\sum_i x_i·y_i
 \]
 \item<+-> Dieses definiert die euklidische Norm („Länge eines Vektors“ per „Satz des Pythagoras“):
 \[
  \|\vec{x}\| ≔ \sqrt{\vec{x}·\vec{x}} = \sqrt{\sum_i x_i^2}
 \]
 \item<+-> In manchen Aufgaben sind andere Normen von Bedeutung, etwa:
 \[
  \|\vec{x}\|_1 ≔ \sum_i |x_i|\text{ (Manhattan-Norm)}
 \]\[
  \|\vec{x}\|_\infty ≔ \max_i |x_i|\text{ (Maximumsnorm)}
 \]
 \item<+-> Jede Norm induziert eine Metrik (Abstandsbegriff):
 \[
  d(\vec{x},\vec{y}) ≔ \|\vec{x}-\vec{y}\|
 \]
\end{itemize}

}

\subsection{Winkel}
\frame{
\begin{block}{Problem}
 Berechne Maß des eingeschlossenen Winkels $φ$ zwischen Vektoren $\vec{a},\vec{b}$.
\end{block}
\pause
\begin{block}{Allgemeine Lösung}
  \[
   \cos φ = \frac{\vec{a}\cdot\vec{b}}{|\vec{a}|·|\vec{b}|} ⇔
   φ = \acos\left(\frac{\vec{a}\cdot\vec{b}}{|\vec{a}|·|\vec{b}|}\right)
  \]
  → Vorsicht mit Fließkommazahlen
\end{block}

  \pause
\begin{block}{Lösung in zwei Dimensionen, $\vec{a}$ positiv auf der $x$-Achse}
  \[
    \tan φ = \frac{b_y}{b_x} ⇔ φ = \atantwo(b_y,b_x)
  \]
\end{block}
}

\subsection{Orientierung}
\frame{
  \begin{itemize}
   \item<+-> Ist ein Winkel kleiner oder größer $π$? Ist eine Drehung im oder gegen den Uhrzeigersinn?
  \[
   ccw(\vec{a},\vec{b}) ≔ (a_x·b_y - a_y·b_x > 0)
  \]
  \item<+-> Kolinearität beim Zwischenergebnis 0
  \item<+-> Mit anderem Ursprung:
  \[
   ccw(\vec{p},\vec{a},\vec{b}) ≔ ccw(\vec{a}-\vec{p},\vec{b}-\vec{p})
  \]
  \end{itemize}
}

\subsection{Geradenschnitt}
\frame{
\begin{itemize}
 \item<+-> Punkte, Geraden, Ebenen etc. sind Nullstellenmengen linearer Gleichungssysteme.
 \item<+-> Der Schnitt zweier solcher Punktmengen wird beschrieben durch die Vereinigung der Gleichungssysteme
 \item<+-> Zum Beispiel gegeben zwei Geraden:  \[
  2x+y=0\]
  \[
  x-y+3=0
 \]
 \item<+-> Schnittpunkt $(-1,2)$
\end{itemize}
}

\subsection{Streckenschnitt}
\frame{
\begin{block}{1. Möglichkeit}
 Man bestimme den Geradenschnitt und überprüfe, ob der Schnittpunkt auf den Strecken liegt
\end{block}
\begin{block}{2. Möglichkeit}
  Funktioniert auch mit ganzen Zahlen. Problem, falls parallele Strecken auftreten.
 \[
  schnitt(\vec{a_1},\vec{a_2},\vec{b_1},\vec{b_2}) ≔ (ccw(\vec{a_1},\vec{a_2},\vec{b_1})≠ccw(\vec{a_1},\vec{a_2},\vec{b_2}))
  \]\[∧(ccw(\vec{b_1},\vec{b_2},\vec{a_1})≠ccw(\vec{b_1},\vec{b_2},\vec{a_2}))
 \]
\end{block}
}

\section{Konvexe Hülle}
\subsection{Grahams Algorithmus}
\frame{
\begin{itemize}
 \item<+-> Wähle Punkt, der garantiert auf dem Rand der konvexen Hülle liegt
 \item<+-> Sortiere Punkte gegen den Uhrzeigersinn ausgehend von diesem Punkt
 \item<+-> Gehe in dieser Reihenfolge die Punkte durch und eliminiere überflüssige
\end{itemize}
\pause
\begin{block}{Punkt, der garantiert in der Hülle liegt?}
 Punkt mit minimalen Koordinaten (etwa bzgl. x und nachrangig bzgl. y)
\end{block}
\begin{block}{Sortieren}
 → Verwende \code{ccw}
\end{block}
}

\subsection{Implementierung}
\begin{frame}[fragile]
\begin{lstlisting}
struct comparc {
  point p;
  inline bool operator()(point x, point y) {
    if(y == p) return false;
    if(x == p) return true;
    return ccw(p, x, y);
  }
} comp; ... comp.p = base;
sort(points.begin(), points.end(), comp);
points.push_back(base);
vector<int> res;
for(int pos = 0; pos <= n; ++pos) {
    while(res.size() >= 2 
      && ccw(points[res[res.size()-2]],
         points[pos], points[res[res.size()-1]]))
            res.pop_back();
    if(pos != n) res.push_back(pos);
}
\end{lstlisting}
\end{frame}

\section{Sweep Line}
\subsection{Nächste Nachbarn}
\frame{
\begin{itemize}
 \item<+-> Problem: Aus einer Punktmenge finde die zwei nächsten Punkte
 \item<+-> Gehe Punkte sortiert nach $x$-Koordinate durch
 \item<+-> Ein Vergleich ist immer nur mit solchen nötig, die auf der $x$- und $y$-Achse näher sind als das bisherige Minimum (euklidische Norm ist größer als Maximumsnorm)
 \item<+-> Verwaltung dieser Mengen auf der $x$-Achse: Über den Sweep
 \item<+-> Verwaltung auf der $y$-Achse: Binärbaum
\end{itemize}
}

\subsection{Implementierung}
\begin{frame}[fragile]
\begin{lstlisting}
set<point, bool(*)(point,point)> status(&cmp);
sort(points.begin(), points.end());
real opt = 1e30; real wurzelopt = 1e15;
iter left = right = points.begin();
status.insert(*right); ++right;
\end{lstlisting}
\end{frame}
\begin{frame}[fragile]
\begin{lstlisting}
while(right != points.end()) {
    if(fabs(left->first-right->first) >= wurzelopt) {
        status.erase(*(left++));
    } else {
        siter lower = status.lower_bound(
            point(-1e20, right->second-wurzelopt));
        siter upper = status.upper_bound(
            point(-1e20, right->second+wurzelopt));
        while(lower != upper) {
            real cand = dist2(*right, *lower);
            if(cand < opt) {
                opt = cand; wurzelopt = sqrt(opt);
            }
            ++lower;
        }
        status.insert(*(right++));
    }
}
\end{lstlisting}
\end{frame}

\subsection{Schnitte}
\frame{
\begin{block}{Problem}
\begin{itemize}
 \item<+-> Gegeben: Eine Menge vertikaler und eine Menge horizontaler Strecken
 \item<+-> Gesucht: Alle Schnittpunkte
\end{itemize}
\end{block}
\pause
\begin{block}{Idee}
\begin{itemize}
 \item<+-> Sweep Line besucht nur die Stellen, an denen vertikale Strecken zu finden sind oder horizontale Strecken beginnen oder enden.
 \item<+-> Verwalte zu betrachtende horizontale Strecken
 \item<+-> An den jeweiligen Positionen werden (in welcher Reihenfolge?):
 \begin{itemize}
  \item<+-> Anfangende horizontale Strecken zur Menge hinzugefügt
  \item<+-> Schnitte mit allen vertikalen Strecken bestimmt
  \item<+-> Endende horizontale Strecken entfernt
 \end{itemize}
\end{itemize}
\end{block}
}

\begin{frame}[fragile]
\begin{block}{Ereignisse}
\begin{lstlisting}
struct event {
    int id;
    real x, y, len;
    enum {
      hbegin, // Links abgeschlossen
      hend,   // Rechts offen
      vert
    } type;
    inline bool operator<(const event &o) const {
      if(x < o.x) return true;
      if(o.x < x) return false;
      if(type < o.type) return true;
      if(o.type < type) return false;
      return id < o.id;
    }
}
\end{lstlisting}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Implementierung}
 \begin{itemize}
  \item<+-> Erstelle und sortiere die Ereignisse (eins für vertikale, zwei für horizontale Strecken)
  \item<+-> Gehe in dieser Reihenfolge die Ereignisse durch
  \item<+-> Verwalte ein \code{set<real>}
  \begin{itemize}
   \item<+-> \code{hbegin}-Ereignis: Füge y-Koordinate und ID ein
   \item<+-> \code{vert}-Ereignis: Stelle Bereichsabfrage
   \item<+-> \code{hend}-Ereignis: Entferne y-Koordinate und ID
  \end{itemize}
 \end{itemize}

\end{block}

\end{frame}



\section{Divide \& Conquer}
\subsection{Sichtbare Segmente}
\frame{
\begin{block}{Problem}
 Gegeben stückweise lineare Funktionen $f_i$. Bestimme die stückweise lineare Funktion $x\mapsto \min_i f_i(x)$.
\end{block}
\pause
\begin{block}{Lösung}
 \begin{itemize}
  \item<+-> Für je zwei stückweise lineare Funktionen lässt sich das Minimum leicht per Sweep berechnen.
  \item<+-> Bestimme Minima per Divide \& Conquer
  \item<+-> Laufzeit in Abhängigkeit der Anzahl der Segmente:
  \[
   \mathcal{O}(n\log n\ α(n))
  \]
 \end{itemize}

\end{block}


}

\section{Implementierungshinweise}
\subsection{Fließkommazahlen}
\frame{
\begin{itemize}
 \item<+-> Wenn es geht, Ganzzahlen nutzen
 \item<+-> Niemals Fließkommazahlen auf Gleichheit prüfen
 \item<+-> Aufgaben, in denen Fließkommazahlen nötig sind, vermeiden singuläre Fälle
 \item<+-> \code{float} meist zu ungenau, \code{long double} ist Versuch wert (aber langsam)
 \item<+-> Überraschungen mit Optimierungen (z. B. wegen erhöhter Genauigkeit in Registern) möglich
\end{itemize}
}

\section{Ausblick}
\subsection{Komplexere Probleme}
\frame{
\begin{itemize}
 \item<+-> Segmentbäume als Sweepstatusstruktur
 \item<+-> Voronoi-Diagramme → $Θ(n\log n)$
 \item<+-> Andere Metriken
 \item<+-> Durch Polynome beschriebene Figuren
 \item<+-> Höhere Dimensionen
\end{itemize}
}

\section{Aufgaben}
\subsection{Polygon}
\frame{
\center{Gegeben ein Polygon: Liegt ein gegebener Punkt in dem Polygon?}
}
\subsection{NWERC 11 I}
\frame{
\begin{center}
\includegraphics[scale=0.35]{rfid.png}
\end{center}
}
\subsection{Sichtbarkeit}
\frame{
\center{Für eine Menge von Strecken finde man alle Teilstrecken, die von einem Punkt aus sichtbar sind.}
}
\subsection{Schnitte}
\frame{
\center{Für eine Menge von Strecken finde man alle Schnittpunkte.}
}

\section{Referenzen}
\nocite{*}
\bibliographystyle{ieeetr}
\frame{\bibliography{geometrie}}

\blackframe

\end{document}
