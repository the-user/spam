/****************************************************************************
 **           - Copyright (C) 2013 by Jonathan Schmidt-Dominé -            **
 **                                  ----                                  **
 **   - This program is free software: you can redistribute it and/or -    **
 ** - modify it under the terms of the GNU Affero General Public License - **
 ** - as published by the Free Software Foundation, either version 3 of -  **
 **        - the License, or (at your option) any later version. -         **
 **                                  ----                                  **
 **  - This program is distributed in the hope that it will be useful, -   **
 **   - but WITHOUT ANY WARRANTY; without even the implied warranty of -   **
 **  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU -  **
 **          - Affero General Public License for more details. -           **
 **                                  ----                                  **
 **  - You should have received a copy of the GNU Affero General Public -  **
 **            - License along with this program. If not, see -            **
 **                  - <http://www.gnu.org/licenses/>. -                   **
 ***************************************************************************/

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <cstdio>
using namespace std;

typedef int real;
typedef pair<real,real> point;

inline point diff(point x, point y) {
	return make_pair(x.first-y.first, x.second-y.second);
}

inline bool ccw(point x, point y) {
	return (x.first*y.second)-(x.second*y.first) > 0;
}

inline bool ccw(point p, point x, point y) {
	return ccw(diff(x,p), diff(y,p));
}

struct comparc {
	point p;
	inline bool operator()(point x, point y) {
		if(y == p) return false;
		if(x == p) return true;
		return ccw(p, x, y);
	}
} comp;

void dump(const vector<int> &res, const vector<point> &points, bool showpos = false) {
	int n = points.size()-1;
	point p;
	for(int i = -10; i != 11; ++i) {
		for(int j = -10; j != 11; ++j) {
			p = make_pair(i,j);
			for(int k = 0; k != n; ++k) {
				if(points[k] == p) {
					for(int l = 0; l != res.size(); ++ l) {
						if(res[l] == k) {
							if(l == res.size()-1)
								cout << "b";
							else if(l == res.size()-2)
								cout << "a";
							else
								cout << "x";
							goto found;
						}
					}
					if(showpos)
						cout << k;
					else
						cout << "*";
					goto found;
				}
			}
			cout << "_";
			found:;
		}
		cout << endl;
	}
}

int main()
{
	int n;
	scanf("%d", &n);
	
	point base = make_pair(1000000000, 1000000000);
	vector<point> points(n);
	
	for(int i = 0; i != n; ++i)
	{
		scanf("%d %d", &(points[i].first), &(points[i].second));
		if(points[i].second < base.second
			|| ((points[i].second == base.second) && (points[i].first < base.first))
			)
		{
			base = points[i];
		}
	}
	
	comp.p = base;
	sort(points.begin(), points.end(), comp);
	points.push_back(base);
	vector<int> res;
	
	dump(res, points, true);
	
	for(int pos = 0; pos <= n; ++pos) {
		dump(res, points);
		while(res.size() >= 2 
			&& ccw(points[res[res.size()-2]],
				   points[pos], points[res[res.size()-1]]))
			res.pop_back();
		if(pos != n) res.push_back(pos);
	}
	cout << points.size() << endl << res.size() << endl;
	

}