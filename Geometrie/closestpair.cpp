/****************************************************************************
 * *           - Copyright (C) 2013 by Jonathan Schmidt-Dominé -            **
 **                                  ----                                  **
 **   - This program is free software: you can redistribute it and/or -    **
 ** - modify it under the terms of the GNU Affero General Public License - **
 ** - as published by the Free Software Foundation, either version 3 of -  **
 **        - the License, or (at your option) any later version. -         **
 **                                  ----                                  **
 **  - This program is distributed in the hope that it will be useful, -   **
 **   - but WITHOUT ANY WARRANTY; without even the implied warranty of -   **
 **  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU -  **
 **          - Affero General Public License for more details. -           **
 **                                  ----                                  **
 **  - You should have received a copy of the GNU Affero General Public -  **
 **            - License along with this program. If not, see -            **
 **                  - <http://www.gnu.org/licenses/>. -                   **
 ***************************************************************************/

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <cmath>
using namespace std;

typedef double real;

typedef pair<real,real> point;

template<typename T>
inline T sqr(const T &x) {
	return x*x;
}


inline real dist2(point x, point y) {
	return sqr(x.first-y.first)+sqr(x.second-y.second);
}

struct reverseless
{
	inline bool operator()(point p, point q) {
		if(p.second == q.second)
			return p.first < q.first;
		return p.second < q.second;
	}
};

inline bool cmp(point p, point q) {
	if(p.second == q.second)
		return p.first < q.first;
	return p.second < q.second;
}

int main()
{
	vector<point> points;
	int n;
	cin >> n;
	points.resize(n);
	for(int i = 0; i != n; ++i)
		cin >> points[i].first >> points[i].second;
	
// 	set<point, reverseless> status;
// 	set<point, __typeof__(&cmp)> status(&cmp);
	set<point, bool(*)(point,point)> status(&cmp);
	typedef vector<point>::iterator iter;
	typedef set<point, bool(*)(point,point)>::iterator siter;
// 	typedef typeof(points.begin()) iter;
// 	typedef typeof(status.begin()) siter;
	
	sort(points.begin(), points.end());
	real opt = 1e30; real wurzelopt = 1e15;
	iter left = points.begin(), right = points.begin();
	status.insert(*right); ++right;

	while(right != points.end()) {
		if(fabs(left->first-right->first) >= wurzelopt) {
			status.erase(*(left++));
		} else {
			siter lower = status.lower_bound(
				point(-1e20, right->second-wurzelopt));
			siter upper = status.upper_bound(
				point(-1e20, right->second+wurzelopt));
			while(lower != upper) {
				real cand = dist2(*right, *lower);
				if(cand < opt) {
					opt = cand; wurzelopt = sqrt(opt);
				}
				++lower;
			}
			status.insert(*(right++));
		}
	}
	
	cout << opt << endl;
	
	opt = 1e30;
	int oi, oj;
	for(int i = 0; i != n; ++i) {
		for(int j = i+1; j != n; ++j) {
			opt = min(opt, dist2(points[i], points[j]));
			oi = i; oj = j;
		}
	}
	cout << opt << endl;
	cout << points[oi].first << " " << points[oi].second << endl;
	cout << points[oj].first << " " << points[oj].second << endl;
}