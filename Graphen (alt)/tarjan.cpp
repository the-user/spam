#include <vector>
#include <stack>
#include <iostream>
#include <algorithm>
using namespace std;

stack<int> s;
int cnt=0;
int scnt=0;
vector<int> back;
vector<int> comp;
vector<bool> visited;
int n,m;
vector<vector<int> > g;

void dfs(int v)
{
    visited[v] = true;
    int x = back[v] = cnt++;
    
    s.push(v);
    
    for(int i = 0; i < g[v].size(); ++i)
    {
    	int w = g[v][i];
        if(!visited[w])
            dfs(w);
        x = min(x, back[w]);
    }
    
    if(x < back[v])
    {
        back[v] = x;
        return;
    }
    
    int t;
    do
    {
        t = s.top(); s.pop();
        comp[t] = scnt;
        back[t] = 999999999;
    }while(t != v);
    
    scnt++;
}


int main()
{
	cin >> n >> m;
	g.resize(n);
	visited.resize(n, false);
	back.resize(n);
	comp.resize(n);
	
	for(int i = 0; i < m; ++i)
	{
		int a,b;
		cin >> a >> b;
		g[a].push_back(b);
	}
	
	for(int i = 0; i < n; ++i)
		if(!visited[i])
			dfs(i);
	for(int i = 0; i < n; ++i)
		cout << i << ": " << comp[i] << endl;
}

