void tue(int nr) {

if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
if(settings.render < 0) settings.render=4;
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);
defaultpen(fontsize(8));
currentpicture=new picture;

import patterns;
size(5cm);
int x1=5,x2=9,y1=3,y2=5;
picture dd;
fill(dd,scale(5)*unitsquare,opacity(0));
fill(dd,shift(3,3)*scale(1)*unitcircle,green);
add("hat1",dd);
erase(dd);
fill(dd,scale(5)*unitsquare,opacity(0));
fill(dd,shift(3,1)*scale(1)*unitcircle,red);
add("hat2",dd);
erase(dd);
fill(dd,scale(5)*unitsquare,opacity(0));
fill(dd,shift(1,3)*scale(1)*unitcircle,red);
add("hat3",dd);
erase(dd);
fill(dd,scale(5)*unitsquare,opacity(0));
fill(dd,shift(1,1)*scale(1)*unitcircle,green);
add("hat4",dd);
erase(dd);
label("0",(0.5,-0.5));
label("0",(-0.5,0.5));
label("$x_1$",(x1+0.5,-0.5));
label("$x_2$",(x2-0.5,-0.5));
label("$y_1$",(-0.5,y1+0.5));
label("$y_2$",(-0.5,y2-0.5));
if (nr >= 1)
	fill((0,0)--(x2,0)--(x2,y2)--(0,y2)--cycle,pattern("hat1"));
if (nr >= 2)
	fill((0,0)--(x1,0)--(x1,y2)--(0,y2)--cycle,pattern("hat2"));
if (nr >= 3)
	fill((0,0)--(x2,0)--(x2,y1)--(0,y1)--cycle,pattern("hat3"));
if (nr >= 4)
	fill((0,0)--(x1,0)--(x1,y1)--(0,y1)--cycle,pattern("hat4"));
draw((x1,y1)--(x2,y1)--(x2,y2)--(x1,y2)--cycle,blue+linewidth(1));

viewportsize=(307.28987pt,0);
shipout(prefix="summe2d-"+string(nr), format="pdf");

}

tue(0);
tue(1);
tue(2);
tue(3);
tue(4);
