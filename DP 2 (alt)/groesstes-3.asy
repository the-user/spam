void tue(int nr) {

if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
if(settings.render < 0) settings.render=4;
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);
defaultpen(fontsize(8));
currentpicture=new picture;
size(5.5cm);

int b[] = {0,3,7,5,9,0,3,1,0};
draw(shift(-2.5,1)*scale(2.5,1)*unitsquare);
draw(shift(-2.5,0)*scale(2.5,1)*unitsquare);
label("$x$",(-0.5,1.5));
label("$t(x,y)$",(-1,0.5));
for (int i = 0; i < b.length; ++i) {
	if (i == nr)
		fill(shift(i,0)*scale(1,2)*unitsquare,blue);
	draw(shift(i,1)*unitsquare);
	draw(shift(i,0)*unitsquare);
	label(string(i),(i+0.5,1.5));
	label(string(b[i]),(i+0.5,0.5));
}

label("Stack:",(-4,-3));
draw(shift(-2.5,-3)*scale(2.5,1)*unitsquare);
draw(shift(-2.5,-4)*scale(2.5,1)*unitsquare);
label("$x$",(-0.5,-2.5));
label("$t(x,y)$",(-1,-3.5));
pair st[];
real c[];
for (int i = 0; i <= nr; ++i) {
	if (i == nr) {
		for (int k = 0; k < st.length; ++k) {
			if (st[k].y < b[nr] && (k == st.length-1 || st[k+1].y >= b[nr]))
				fill(shift(k,-4)*scale(1,2)*unitsquare,blue);
			if (st[k].y >= b[nr])
				fill(shift(k,-4)*scale(1,2)*unitsquare,red);
			draw(shift(k,-3)*unitsquare);
			draw(shift(k,-4)*unitsquare);
			label(string(st[k].x),(k+0.5,-2.5));
			label(string(st[k].y),(k+0.5,-3.5));
		}
		filldraw(shift(st.length,-3)*unitsquare,green);
		filldraw(shift(st.length,-4)*unitsquare,green);
		label(string(nr),(st.length+0.5,-2.5));
		label(string(b[nr]),(st.length+0.5,-3.5));
	}
	while(st.length >= 1 && st[st.length-1].y >= b[i])
		st.pop();
	c.push(st.length > 0 ? st[st.length-1].x : -1);
	st.push((i,b[i]));
	if (b[i] != 0) {
		if (i == nr)
			fill(shift(i,-1)*unitsquare,blue);
		draw(shift(i,-1)*unitsquare);
		label(string(i-c[i]),(i+0.5,-0.5));
	}
}

viewportsize=(307.28987pt,0);
shipout(prefix="groesstes-3-"+string(nr), format="pdf");

}

for (int i = 0; i <= 8; ++i)
	tue(i);
