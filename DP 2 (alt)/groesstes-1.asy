if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
if(settings.render < 0) settings.render=4;
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);
defaultpen(fontsize(8));
currentpicture=new picture;
size(4cm);

int X = 10, Y = 10;

srand(1);
bool a[][];
int  b[][];
for (int x = 0; x < X; ++x) {
	bool ax[];
	int  bx[];
	int l = 0;
	for (int y = 0; y < Y; ++y) {
		ax.push(unitrand() <= 0.8 && x != 0 && x != X-1 && y != 0 && y != Y-1);
		if (!ax[y])
			bx.push(0);
		else
			bx.push(l+1);
		l = bx[y];
	}
	a.push(ax);
	b.push(bx);
}

for (int x = 0; x < X; ++x) {
	for (int y = 0; y < Y; ++y) {
		if (!a[x][y])
			fill(shift(x,-y)*unitsquare,lightred);
		draw(shift(x,-y)*unitsquare);
		label(string(b[x][y]),(x+0.5,-y+0.5));
	}
}

viewportsize=(307.28987pt,0);
shipout(prefix="groesstes-1", format="pdf");
