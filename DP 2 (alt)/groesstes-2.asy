if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
if(settings.render < 0) settings.render=4;
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);
defaultpen(fontsize(8));
currentpicture=new picture;
size(4.5cm);

int x1 = 0, x2 = 5, x3 = 8, y1 = 0, y2 = 6, y3 = 2, y4 = 5;

filldraw(shift(x2,y2)*unitsquare,lightred);
filldraw(shift(x1-1,y3)*unitsquare,lightred);
filldraw(shift(x3,y4)*unitsquare,lightred);
fill(shift(x2,y1)*unitsquare,lightgray);
draw(shift(x1,y1)*scale(x3-x1,y2-y1)*unitsquare,linewidth(1));
draw(shift(x1-1,y1)*unitsquare);
draw(shift(x3,y1)*unitsquare);
draw((x2+1,y1)--(x2+1,y2));
draw((x2,y1)--(x2,y2));
//draw(shift(x2,y1)*unitsquare);
draw((x1,y1+1)--(x3,y1+1));
label("$s$",(x2+0.5,y1+0.5));
label("$\geq s$",((x2+x3+1)/2,y1+0.5));
label("$\geq s$",((x1+x2)/2,y1+0.5));
label("$<s$",(x1-0.5,y1+0.5));
label("$<s$",(x3+0.5,y1+0.5));

viewportsize=(307.28987pt,0);
shipout(prefix="groesstes-2", format="pdf");
