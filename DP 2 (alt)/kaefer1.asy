void tue(int nr) {
real kaeferpos = 3;
bool getrunken[] = {false,false,true,false,true,false,true,false};
real tropfenpos[] = {-5,-4,-3,1,2,5,7,9};
if (nr >= 1) {
	for (int i = 2; i < 7; ++i) {
		getrunken[i] = true;
	}
}
if (nr >= 2) {
	kaeferpos = tropfenpos[6];
}

if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
if(settings.render < 0) settings.render=4;
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);
defaultpen(fontsize(8));
currentpicture=new picture;
size(5.5cm);

for(int i = 0; i < tropfenpos.length; ++i) {
	dot((tropfenpos[i],0),getrunken[i] ? green : blue);
}
dot((kaeferpos,0.1),red);

shipout(prefix="kaefer1-"+string(nr), format="pdf");
}

tue(0);
tue(1);
tue(2);
